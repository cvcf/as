# As

As is a Common Lisp library that enables clean, common-sense conversions between
types.

## Why not use `coerce`?

The primary reason to use `as` as opposed to `coerce` is that `coerce` is
limited in its applications. First, types for `coerce` must refer to actual
types. Second, `coerce` cannot be extended to handle user-defined types or
concepts that do not have actual type definitions (i.e. CL does not have data
types corresponding to `set`s, `plist`s, `alist`s, etc).

Consider the following scenarios:
1. What if you want to convert a plist to a hash-table?
1. What if you want to convert a list to a set?
1. What if you want to convert a hash table to a user-defined object?

None of these things can be accomplished with `coerce`\*. However, *all* of
these things can be achieved with `as` because `as` is defined as a generic
function so methods can be defined that specialize on user-defined types,
and concepts that are not explicitly represented by specific data types in CL -
e.g. `set`s, etc. In essense, `as` is intended to cover a super-set of what
`coerce` covers.

Finally, the guiding principle for `as` is that it is a common-sense conversion
utility. That is, it should "pretty much do what you would expect" and if it
doesn't, you can define a method that does what you would expect.

\* It's possible that I'm wrong about this but, from what I've seen so far, none
of these things are possible using `coerce`.

## Examples

Here are a couple examples of the kinds of things one might do with `as`.

```lisp
(asdf:load-system :as)
(in-package #:as)

(as :list "abcd")
; -> (#\a #\b #\c #\d)

(as :map '(:a 1 :b 2 :c '(hello world)))
; -> #<HASH-TABLE :TEST EQL :COUNT 3 {1007DB5333}>

(as :set '(a b c a b d a c d))
; -> (b a c d)
```
