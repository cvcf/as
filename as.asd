(defsystem #:as
  :description "A library that enables clean conversions between types."
  :version "0.0.1"
  :author "Cameron V Chaparro <cameron@chaparroconsulting.group>"
  :license "GPL3"
  :serial t
  :pathname "src/"
  :components ((:file "package")
               (:file "utils")
               (:file "as"))
  :in-order-to ((test-op (test-op #:as/tests))))

(defsystem #:as/tests
  :description "Test system for the `as' system"
  :version "0.0.1"
  :depends-on (#:fiveam
               #:as)
  :serial t
  :pathname "t/"
  :components ((:file "package")
               (:file "as-tests"))
  :perform (test-op (o c) (uiop:symbol-call '#:as/tests '#:run-all-tests)))
