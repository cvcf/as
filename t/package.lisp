(defpackage #:as/tests
  (:use #:cl #:as #:fiveam)
  (:export #:all-tests))

(in-package #:as/tests)

(def-suite all-tests
  :description "All the `AS' tests.")
