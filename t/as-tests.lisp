(in-package #:as/tests)

(def-suite as-list
  :description "Convert objects to lists"
  :in all-tests)
(in-suite as-list)

(test convert-to-list
  (is (equal (as :list nil)       nil))
  (is (equal (as :list '())       nil))
  (is (equal (as :list 5)         '(5)))
  (is (equal (as :list 4)         '(4)))
  (is (equal (as :list #\a)       '(#\a)))
  (is (equal (as :list 'atom)     '(atom)))
  (is (equal (as :list #'atom)    `(,#'atom)))
  (is (equal (as :list '(a))      '(a)))
  (is (equal (as :list "hello")   '(#\h #\e #\l #\l #\o)))
  (is (equal (as :list #(0 0 1))  '(0 0 1)))

  (with-hash-table (:a 1 :b 2)
    (is (equal (as :list table) '(:a 1 :b 2)))))

(def-suite as-hash-table
  :description "Convert objects to hash tables"
  :in all-tests)
(in-suite as-hash-table)

(test convert-to-hash-table
  (dolist (what '(:map))
    (is (zerop (hash-table-count (as what nil))))
    (with-hash-table (:a 1 :b 2)
      (is (= 2 (hash-table-count table)))
      (is (equalp (as what '(:a 1 :b 2)) table)))
    (with-hash-table (:a 1 :b 2)
      (is (= 2 (hash-table-count table)))
      (is (equalp (as what #(:a 1 :b 2)) table)))
    (with-hash-table (#\a #\1 #\b #\2)
      (is (= 2 (hash-table-count table)))
      (is (equalp (as what "a1b2") table)))
    (with-hash-table (:a (list 'hello 'world) :b (list 'good-bye 'world))
      (is (= 2 (hash-table-count table)))
      (is (equalp (as what '(:a '(hello world) :b '(good-bye world))) table)))
    (let ((hello "hi") (good-bye "bye"))
      (with-hash-table (:a (list hello 'world) :b (list good-bye 'world))
        (is (= 2 (hash-table-count table)))
        (is (equalp (as what `(:a '(,hello world) :b '(,good-bye world))) table))
        (is (equal (gethash :a table) (list hello 'world)))
        (is (equal (gethash :b table) (list good-bye 'world)))))))

(def-suite as-set
  :description "Convert objects to sets"
  :in all-tests)
(in-suite as-set)

(test convert-to-set
  (is (equalp (as :set nil)        nil))
  (is (equalp (as :set '(0 0 0))   '(0)))
  (is (equalp (as :set #(0 0 0))   #(0)))
  (is (equalp (as :set "abcdabcd") "abcd")))
