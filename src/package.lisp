(defpackage #:as
  (:use #:cl)
  (:export #:as)
  (:export #:with-hash-table
           #:table))
