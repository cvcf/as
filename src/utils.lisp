(in-package #:as)

(defmacro with-hash-table ((&rest values) &body body)
  "Execute `BODY' where `VALUES' is used to create a hash table.

Each pair of values in `VALUES' is treated as a key-value pair for creating the
hash table.

To access the table, use `TABLE'."
  (let ((values (coerce values 'list)))
    `(let ((table (make-hash-table :size ,(/ (length values) 2))))
       ,@(loop :for key :in values :by #'cddr
               :for val :in (rest values) :by #'cddr
               :collect `(setf (gethash ,key table) ,val))
       ,@body)))
