(in-package #:as)

(defgeneric as (what value)
  (:method (what value)
    (error "Conversion from ~a to ~a is undefined." value what))
  (:documentation "Convert VALUE to an object of type WHAT."))

(defmethod as ((what (eql :list)) (value sequence))
  (coerce value 'list))

(defmethod as ((what (eql :list)) (value hash-table))
  (loop for key being the hash-keys of value
        for val being the hash-values of value
        collect key
        collect val))

(defmethod as ((what (eql :list)) (value array))
  ;; TODO: Expand to work on multi-dimensional arrays
  (loop for x across value collect x))

;; TODO: See if there's a CLOS way to do this without duplicating each method
(defmethod as ((what (eql :list)) (value character)) (list value))
(defmethod as ((what (eql :list)) (value function))  (list value))
(defmethod as ((what (eql :list)) (value number))    (list value))
(defmethod as ((what (eql :list)) (value symbol))    (list value))

(defmethod as ((what (eql :list)) (value list)) value)
(defmethod as ((what (eql :list)) (value null)) value)

(defmethod as ((what (eql :map)) (value sequence))
  ;; TODO: Figure out how to pass `value' as a list to `with-hash-table' without
  ;; having to `eval' it like this.
  (eval `(with-hash-table ,value table)))

(defmethod as ((what (eql :set)) (value sequence))
  (remove-duplicates value :test #'equalp))
